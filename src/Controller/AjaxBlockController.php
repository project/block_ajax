<?php

namespace Drupal\block_ajax\Controller;

use Drupal\block\BlockInterface;
use Drupal\block_ajax\BlockViewBuilder;
use Drupal\Component\Utility\Xss;
use Drupal\Core\Block\BlockPluginInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\block_ajax\Response\AjaxBlockResponse;
use Drupal\Core\Block\BlockManager;
use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Render\RendererInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\Utility\Token;
use Drupal\node\NodeInterface;
use Drupal\taxonomy\TermInterface;
use Drupal\user\UserInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\Request;

/**
 * Ajax Blocks Controller.
 */
class AjaxBlockController extends ControllerBase {

  /**
   * The block manager service.
   *
   * @var \Drupal\Core\Block\BlockManager
   */
  protected $blockManager;

  /**
   * The renderer service.
   *
   * @var \Drupal\Core\Render\RendererInterface
   */
  protected $renderer;

  /**
   * Entity type manager service.
   *
   * @var \Drupal\Core\Entity\EntityTypeManager
   */
  protected $entityTypeManager;

  /**
   * Block view builder service.
   *
   * @var \Drupal\block_ajax\BlockViewBuilder
   */
  protected $blockViewBuilder;

  /**
   * The token service.
   *
   * @var \Drupal\Core\Utility\Token
   */
  protected $token;

  /**
   * The current user.
   *
   * @var \Drupal\Core\Session\AccountInterface
   */
  protected $account;

  /**
   * Block Ajax Controller Constructor.
   *
   * @param \Drupal\Core\Block\BlockManager $block_manager
   *   The block manager service.
   * @param \Drupal\Core\Render\RendererInterface $renderer
   *   The renderer service.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   Entity type manager service.
   * @param \Drupal\block_ajax\BlockViewBuilder $block_view_builder
   *   Block view builder service.
   * @param \Drupal\Core\Utility\Token $token
   *   The token.
   * @param \Drupal\Core\Session\AccountInterface $account
   *   The current user account.
   */
  public function __construct(
    BlockManager $block_manager,
    RendererInterface $renderer,
    EntityTypeManagerInterface $entity_type_manager,
    BlockViewBuilder $block_view_builder,
    Token $token,
    AccountInterface $account
  ) {
    $this->blockManager = $block_manager;
    $this->renderer = $renderer;
    $this->entityTypeManager = $entity_type_manager;
    $this->blockViewBuilder = $block_view_builder;
    $this->token = $token;
    $this->account = $account;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container): AjaxBlockController {
    return new static(
      $container->get('plugin.manager.block'),
      $container->get('renderer'),
      $container->get('entity_type.manager'),
      $container->get('block_ajax.block_view_builder'),
      $container->get('token'),
      $container->get('current_user')
    );
  }

  /**
   * Get the block instance.
   *
   * @param \Symfony\Component\HttpFoundation\Request $request
   *   The request.
   * @param string $block_id
   *   The block/plugin id.
   * @param array $configuration
   *   The block configuration.
   *
   * @return array|null
   *   Returns the block render array/instance.
   *
   * @throws \Drupal\Component\Plugin\Exception\ContextException
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\MissingValueContextException
   * @throws \Drupal\Component\Plugin\Exception\PluginException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  protected function getBlockInstance(Request $request, string $block_id, array $configuration): ?array {
    $blockInstance = NULL;

    // Load block by block_id.
    $block = $this->entityTypeManager->getStorage('block')->load($block_id);
    if (!$block instanceof BlockInterface) {
      // Attempt to load block plugin.
      $block_plugin = $this->blockManager->createInstance($block_id, $configuration);
      if ($block_plugin instanceof BlockPluginInterface && 'broken' !== $block_plugin->getPluginId()) {
        // Check access.
        $access = $block_plugin->access($this->account, TRUE);
        if ($access->isAllowed()) {
          $blockInstance = $block_plugin->build();
          $blockInstance['#cache']['tags'] = array_merge($block_plugin->getCacheTags(), ['block_ajax']);
        }
      }
    }
    else {
      // Add in custom cache tag.
      $block->addCacheTags(['block_ajax']);

      // Make sure we have plugin id.
      $plugin_id = $request->get('plugin_id', '');
      if (!empty($plugin_id)) {
        // Check if block has special plugin and add it to dependency.
        $plugin = $block->getPlugin();
        if (is_object($plugin) && $plugin->getPluginId() == $plugin_id) {
          $configuration = $plugin->getConfiguration();
        }

        // Construct and render the block.
        $blockInstance = $this->blockViewBuilder->build($plugin_id, $configuration, TRUE);
      }
    }

    return $blockInstance;
  }

  /**
   * Set response max age.
   *
   * @param \Drupal\block_ajax\Response\AjaxBlockResponse $response
   *   The ajax response.
   * @param array $configuration
   *   The block configuration.
   */
  protected function setResponseMaxAge(AjaxBlockResponse &$response, array $configuration) {
    $max_age = 0;
    if (isset($configuration['block_ajax']['max_age'])) {
      $max_age = $configuration['block_ajax']['max_age'];
    }
    $response->setMaxAge($max_age);
    $response->setSharedMaxAge($max_age);
  }

  /**
   * Get block configuration.
   *
   * @param \Symfony\Component\HttpFoundation\Request $request
   *   The request.
   *
   * @return array
   *   The merged and sanitized configuration.
   */
  protected function getBlockConfiguration(Request $request): array {
    // The default configuration.
    $configuration = $request->get('config', []);
    // Filter/sanitize configuration data.
    return $this->filterConfiguration($configuration);
  }

  /**
   * Get filtered block configuration data.
   *
   * @param array $data
   *   The configuration data.
   *
   * @return array
   *   The filtered configuration data.
   */
  protected function filterConfiguration(array $data): array {
    foreach ($data as $key => $value) {
      if (is_array($value)) {
        $this->filterConfiguration($value);
      }
      else {
        $data[$key] = Xss::filter($value);
      }
    }
    return $data;
  }

  /**
   * Implements ajax block update request handler.
   *
   * @param \Symfony\Component\HttpFoundation\Request $request
   *   The request.
   * @param string $block_id
   *   The block id.
   *
   * @return \Drupal\block_ajax\Response\AjaxBlockResponse
   *   Returns the ajax block response.
   *
   * @throws \Drupal\Component\Plugin\Exception\ContextException
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\MissingValueContextException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException|\Drupal\Component\Plugin\Exception\PluginException
   */
  public function loadBlock(
    Request $request,
    string $block_id
  ): AjaxBlockResponse {
    // Response.
    $response = new AjaxBlockResponse();

    // Check for block id.
    if (empty($block_id)) {
      return $response;
    }

    // Block configuration.
    $configuration = $this->getBlockConfiguration($request);

    // Get block instance.
    if ($blockInstance = $this->getBlockInstance($request, $block_id, $configuration)) {
      $renderedBlock = $this->renderer->renderRoot($blockInstance);

      // Apply token replacement.
      $renderedBlock = $this->token->replace($renderedBlock);

      // Clean up ajax links.
      $renderedBlock = str_replace('/block/ajax/' . $block_id, '', trim($renderedBlock));

      // Set data for response.
      $this->setResponseMaxAge($response, $configuration);
      $response->setData([
        'content' => $renderedBlock,
      ]);
    }

    // Returns response.
    return $response;
  }

  /**
   * Implements ajax block update request handler for node context.
   *
   * @param \Symfony\Component\HttpFoundation\Request $request
   *   The request.
   * @param string $block_id
   *   The block id.
   * @param \Drupal\node\NodeInterface $node
   *   The node.
   *
   * @return \Drupal\block_ajax\Response\AjaxBlockResponse
   *   Returns the ajax block response.
   *
   * @throws \Drupal\Component\Plugin\Exception\ContextException
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\MissingValueContextException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException|\Drupal\Component\Plugin\Exception\PluginException
   */
  public function loadBlockNodeContext(
    Request $request,
    string $block_id,
    NodeInterface $node
  ): AjaxBlockResponse {
    // Response.
    $response = new AjaxBlockResponse();

    // Check for block id.
    if (empty($block_id)) {
      return $response;
    }

    // Block configuration.
    $configuration = $this->getBlockConfiguration($request);

    // Get block instance.
    if ($blockInstance = $this->getBlockInstance($request, $block_id, $configuration)) {
      $renderedBlock = $this->renderer->renderRoot($blockInstance);

      // Apply token replacement.
      $renderedBlock = $this->token->replace($renderedBlock, [
        'node' => $node,
      ]);

      // Clean up ajax links.
      $renderedBlock = str_replace('/block/ajax/' . $block_id, '', trim($renderedBlock));

      // Set data for response.
      $this->setResponseMaxAge($response, $configuration);
      $response->setData([
        'content' => $renderedBlock,
      ]);
    }

    // Returns response.
    return $response;
  }

  /**
   * Implements ajax block update request handler for taxonomy term context.
   *
   * @param \Symfony\Component\HttpFoundation\Request $request
   *   The request.
   * @param string $block_id
   *   The block id.
   * @param \Drupal\taxonomy\TermInterface $term
   *   The taxonomy term.
   *
   * @return \Drupal\block_ajax\Response\AjaxBlockResponse
   *   Returns the ajax block response.
   *
   * @throws \Drupal\Component\Plugin\Exception\ContextException
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\MissingValueContextException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException|\Drupal\Component\Plugin\Exception\PluginException
   */
  public function loadBlockTaxonomyTermContext(
    Request $request,
    string $block_id,
    TermInterface $term
  ): AjaxBlockResponse {
    // Response.
    $response = new AjaxBlockResponse();

    // Check for block id.
    if (empty($block_id)) {
      return $response;
    }

    // Block configuration.
    $configuration = $this->getBlockConfiguration($request);

    // Get block instance.
    if ($blockInstance = $this->getBlockInstance($request, $block_id, $configuration)) {
      $renderedBlock = $this->renderer->renderRoot($blockInstance);

      // Apply token replacement.
      $renderedBlock = $this->token->replace($renderedBlock, [
        'term' => $term,
      ]);

      // Clean up ajax links.
      $renderedBlock = str_replace('/block/ajax/' . $block_id, '', trim($renderedBlock));

      // Set data for response.
      $this->setResponseMaxAge($response, $configuration);
      $response->setData([
        'content' => $renderedBlock,
      ]);
    }

    // Returns response.
    return $response;
  }

  /**
   * Implements ajax block update request handler for user context.
   *
   * @param \Symfony\Component\HttpFoundation\Request $request
   *   The request.
   * @param string $block_id
   *   The block id.
   * @param \Drupal\user\UserInterface $user
   *   The user.
   *
   * @return \Drupal\block_ajax\Response\AjaxBlockResponse
   *   Returns the ajax block response.
   *
   * @throws \Drupal\Component\Plugin\Exception\ContextException
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\MissingValueContextException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException|\Drupal\Component\Plugin\Exception\PluginException
   */
  public function loadBlockUserContext(
    Request $request,
    string $block_id,
    UserInterface $user
  ): AjaxBlockResponse {
    // Response.
    $response = new AjaxBlockResponse();

    // Check for block id.
    if (empty($block_id)) {
      return $response;
    }

    // Block configuration.
    $configuration = $this->getBlockConfiguration($request);

    // Get block instance.
    if ($blockInstance = $this->getBlockInstance($request, $block_id, $configuration)) {
      $renderedBlock = $this->renderer->renderRoot($blockInstance);

      // Apply token replacement.
      $renderedBlock = $this->token->replace($renderedBlock, [
        'user' => $user,
      ]);

      // Clean up ajax links.
      $renderedBlock = str_replace('/block/ajax/' . $block_id, '', trim($renderedBlock));

      // Set data for response.
      $this->setResponseMaxAge($response, $configuration);
      $response->setData([
        'content' => $renderedBlock,
      ]);
    }

    // Returns response.
    return $response;
  }

}
