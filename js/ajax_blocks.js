(function ($, Drupal, once) {
  Drupal.behaviors.block_ajax = {
    attach: function (context, drupalSettings) {
      /**
       * Implements ajax block handler.
       */
      let ajaxBlockHandler = function ($block) {
        // Make sure we have block data.
        let blockId = $block.data('block-ajax-id');
        let pluginId = $block.data('block-ajax-plugin-id');
        if (!pluginId) {
          return;
        }

        // Block parent.
        let $blockParent = $block.parent();

        // Setup ajax url.
        let ajaxUrl = 'block/ajax/' + blockId;

        // Load up context if set.
        if (drupalSettings.block_ajax.blocks[blockId].block_ajax.context
          && drupalSettings.block_ajax.blocks[blockId].block_ajax.context.context_type) {
          let contextType = drupalSettings.block_ajax.blocks[blockId].block_ajax.context.context_type;
          let contextArgument = NULL;
          if (contextType === 'node') {
            contextArgument = drupalSettings.block_ajax.blocks[blockId].current_node;
          }
          if (contextType === 'taxonomy_term') {
            contextArgument = drupalSettings.block_ajax.blocks[blockId].current_term;
          }
          if (contextType === 'user') {
            contextArgument = drupalSettings.block_ajax.blocks[blockId].current_user;
          }
          if (contextArgument) {
            ajaxUrl += '/' + contextType + '/' + contextArgument;
          }
        }

        // Setup request.
        let request = $.ajax({
          type: drupalSettings.block_ajax.config['type'],
          url: Drupal.url(ajaxUrl),
          data: {
            plugin_id: pluginId,
            config: drupalSettings.block_ajax.blocks[blockId]
          },
          timeout: drupalSettings.block_ajax.config['timeout'],
          cache: drupalSettings.block_ajax.config['cache'],
          async: drupalSettings.block_ajax.config['async'],
          dataType: drupalSettings.block_ajax.config['dataType'],
          beforeSend: function () {
            if (drupalSettings.block_ajax.blocks[blockId].block_ajax.show_spinner) {
              // Add Ajax progress throbber.
              if (drupalSettings.block_ajax.blocks[blockId].block_ajax.placeholder) {
                // Add throbber with message.
                $block.after(Drupal.theme.ajaxProgressThrobber(Drupal.t(drupalSettings.block_ajax.blocks[blockId].block_ajax.placeholder)));
              } else {
                // Add throbber with no message.
                $block.after(Drupal.theme.ajaxProgressThrobber());
              }
            }
          }
        });

        // Request done handler.
        request.done(function (data) {
          // No content, let's return.
          if (typeof data.content !== 'string' || !data.content.length) {
            return;
          }

          // Add processed class to block wrapper.
          // Remove contextual-region class and move to inside.
          $blockParent.addClass('block-ajax-processed').removeClass('contextual-region');

          // Replacing block contents.
          if (drupalSettings.block_ajax.blocks[blockId].block_ajax.refresh_block) {
            $block.html(data.content);
          } else {
            $block.replaceWith(data.content);
          }

          // Move contextual links inside new block content.
          $('.contextual', $blockParent).prependTo($('.block-ajax-block', $blockParent));

          // Add in contextual-region class.
          $('.block-ajax-block', $blockParent).addClass('contextual-region');

          // Attach behaviours to the updated element.
          $block.each(function () {
            // Note that the global drupal settings reference is being used to
            // make sure the actualised global state is being used.
            Drupal.attachBehaviors(this, window.drupalSettings);
          });

          // Remove throbber.
          if (drupalSettings.block_ajax.blocks[blockId].block_ajax.show_spinner) {
            $('.ajax-progress-throbber', $blockParent).remove();
          }
        });

        // Request fail handler.
        request.fail(function (jqXHR, textStatus) {
          // Remove throbber.
          if (drupalSettings.block_ajax.blocks[blockId].block_ajax.show_spinner) {
            $('.ajax-progress-throbber', $blockParent).remove();
          }

          // Throw console log error.
          console.log("Ajax Block: " + blockId + " request failed: " + textStatus);
        });
      };
      /**
       * Initialize and loop over Ajax blocks.
       */
      $(once('block_ajax', '[data-block-ajax-id]', context)).each(function () {
        let $block = $(this);
        let blockId = $block.data('block-ajax-id');
        if (blockId && drupalSettings.block_ajax.blocks[blockId] !== undefined) {
          // Load block via button.
          if (drupalSettings.block_ajax.blocks[blockId].block_ajax.load_button) {
            $('#block-ajax-button-' + blockId).on('click', function () {
              ajaxBlockHandler($block);
            });
          } else if (drupalSettings.block_ajax.blocks[blockId].block_ajax.refresh_block) {
            // Refresh ajax block on set interval.
            let refreshInterval = drupalSettings.block_ajax.blocks[blockId].block_ajax.refresh_interval;
            setInterval(ajaxBlockHandler, refreshInterval, $block);
          } else {
            // Load in block via AJAX on page load (default operation).
            ajaxBlockHandler($block);
            // On RefreshAjaxBlock event.
            $block.on('RefreshAjaxBlock', function () {
              // Execute the handler payload.
              ajaxBlockHandler($(this));
            });
          }
        }
      });
    },
    detach: function (context) {
    }
  };
  /**
   * Implements ajax block refreshing command.
   */
  Drupal.AjaxCommands.prototype.AjaxBlockRefreshCommand = function (ajax, response, status) {
    $(response.selector).trigger('RefreshAjaxBlock');
  };
})(jQuery, Drupal, once);
